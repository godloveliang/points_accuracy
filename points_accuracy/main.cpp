//for landmark detector
#include "gflags/gflags.h"
#include "glog/logging.h"
#include <iostream>
#include "points_accuracy.h"

int main(int argc, char** argv)
{
    ::google::InitGoogleLogging(argv[0]);
    ::gflags::ParseCommandLineFlags(&argc, &argv, false);
    ros::init(argc, argv, "points_accuracy_node");
    points_accuracy points_accuracy_;

    ROS_INFO("----- come into points_accuracy node -----");
    LOG(INFO) << "----- come into points_accuracy node -----";
    {  
        points_accuracy_.Init();
        ros::spin();
    }
    ROS_INFO("----- out of points_accuracy node -----");
    return 1;
}

