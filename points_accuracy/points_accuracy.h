#ifndef POINTSACCURACY_H_
#define POINTSACCURACY_H_ 

#include <sstream> 
#include <fstream> 
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/PointCloud.h"
#include "sensor_msgs/PointCloud2.h"
#include "glog/logging.h"
#include "gflags/gflags.h"
#include <iostream> 
#include <string>  
#include <vector>
#include <cmath>


class points_accuracy {
public:

    points_accuracy();
    ~points_accuracy();

  void Init() {
    ros::NodeHandle private_nh("~/");
    ros::NodeHandle nh;
    private_nh.param("sub_scan_topic", sub_scan_topic_, std::string("scan"));
    private_nh.param("num_data_need", num_data_need_, 1000);
    private_nh.param("scan_point_angle", scan_point_angle_, 0.);
    points_sub_ = nh.subscribe<sensor_msgs::LaserScan>(sub_scan_topic_, 1000, &points_accuracy::LidarCallBack, this);
  }

  void LidarCallBack(const sensor_msgs::LaserScan::ConstPtr& msg);
  void getdata(const sensor_msgs::LaserScan::ConstPtr& msg, double scan_point_angle);
  void statistics();

private:
  
  ::ros::Subscriber points_sub_;
  std::string sub_scan_topic_;
    
  double mean_range_= 0.;
  double Standard_deviation_range_ = 0.;

  double max_jump_range_;

  int num_data_need_;
  int num_data_get_=0;
  
  
  //record the scan_ranges
  std::vector<double> scan_ranges_;
  //the point need to statistics
  double scan_point_angle_;
  int scan_point_index_ = -1;

  bool print_result_ = true;
};
#endif /* GENERATELANDMARKS_H_ */
