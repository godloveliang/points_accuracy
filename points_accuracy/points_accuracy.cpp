
#include "points_accuracy.h"

points_accuracy::points_accuracy(){}

points_accuracy::~points_accuracy(){}

void points_accuracy::LidarCallBack(const sensor_msgs::LaserScan::ConstPtr& msg){
    if(num_data_get_ < num_data_need_){
        getdata(msg, scan_point_angle_);
    } else if(num_data_get_==num_data_need_ && print_result_){
        statistics();
        std::cout<<"=========================="<<std::endl;
        std::cout<<"=== scan_point_angle_: "<<scan_point_angle_<<std::endl;
        std::cout<<"=== mean_range_: "<<mean_range_<<std::endl;
        std::cout<<"=== Standard_deviation_range_: "<<Standard_deviation_range_<<std::endl;
        std::cout<<"=== max_jump_range_: "<<max_jump_range_<<std::endl;
        std::cout<<"=========================="<<std::endl;
        print_result_ = false;
    }
}
      
void points_accuracy::getdata(const sensor_msgs::LaserScan::ConstPtr& msg, double scan_point_angle){
    if(scan_point_index_ == -1){
        double angle_min = msg->angle_min;
	    double angle_max = msg->angle_max;
	    double angle_increment = msg->angle_increment;
        if(scan_point_angle <= angle_min || scan_point_angle > angle_max){
            LOG(INFO)<<"the scan_point_angle is out of range, should aroud (-180, 180]";
        }else{
            double target_point_angle;
            int ranges = msg->ranges.size();
            for(int i=0; i<ranges; i++){
                target_point_angle = angle_min + i*angle_increment;
                if(abs(target_point_angle - scan_point_angle ) < 0.01 ){
                    scan_point_index_ = i;
                }
            }
        }
        scan_point_index_ +=1;
    }else{
        scan_ranges_.push_back(msg->ranges[scan_point_index_]);
        num_data_get_+=1;
    }
}

void points_accuracy::statistics(){
    for(int i=0; i<num_data_need_; i++){
        mean_range_ += scan_ranges_[i];
    }
    mean_range_ = mean_range_/num_data_need_;
    double min_range= mean_range_;
    double max_range= mean_range_;

    for(int j=0; j<num_data_need_; j++){
        Standard_deviation_range_ += pow(scan_ranges_[j]- mean_range_, 2);
        if(scan_ranges_[j] < min_range) min_range = scan_ranges_[j];
        if(scan_ranges_[j] > max_range) max_range = scan_ranges_[j];
    }
    Standard_deviation_range_=sqrt(Standard_deviation_range_/num_data_need_);
    max_jump_range_ = max_range - min_range;
}