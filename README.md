# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 用于统计2d点云，测距精度。

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* 配置文件位置/param/points_accuracy.yaml
* sub_scan_topic: "scan" :  设置要订阅的2d雷达topic
* num_data_need: 100 : 设置要连续统计的点云数目
* scan_point_angle: 0 ：设置要统计的某根线的角度

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact